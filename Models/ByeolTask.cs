﻿using KnifeZ.Unity.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Windows.Data;

namespace KnifeZ.Byeol.Models
{
    public class ByeolTask : Unity.Models.KnifeBasePoco
    {
        public Guid TaskID { get; set; }

        [StringLength(100)]
        public string TaskName { get; set; }

        [StringLength(100)]
        public string TaskGroupName { get; set; }

        public HttpMethodEnum AskMethodType { get; set; }

        public string AskApi { get; set; }

        public string AskParams { get; set; }

        public string CronLambda { get; set; }
        public int LoopSeconds { get; set; }
        public string Headers { get; set; }

        [StringLength(300)]
        public string Remark { get; set; }

        public bool IsEmail { get; set; }
        public string EmailAddress { get; set; }

        public string CreateBy { get; set; }
        public DateTime CreateTime { get; set; }
        public string UpdateBy { get; set; }
        public DateTime UpdateTime { get; set; }

        /// <summary>
        /// 中断循环
        /// </summary>
        public bool IsInterrupt { get; set; }

        /// <summary>
        /// 下次执行时间
        /// </summary>
        public DateTime NextExecuteTime { get; set; }


    }
    public class ActionTypeConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            HttpMethodEnum s = (HttpMethodEnum)value;
            return s == (HttpMethodEnum)int.Parse(parameter.ToString());
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool isChecked = (bool)value;
            if (!isChecked)
            {
                return null;
            }
            return (HttpMethodEnum)int.Parse(parameter.ToString());
        }
    }
}


