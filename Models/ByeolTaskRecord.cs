﻿using KnifeZ.Unity.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace KnifeZ.Byeol.Models
{
    public class ByeolTaskRecord : KnifeBasePoco
    {
        public Guid TaskID { get; set; }
        public ByeolTask Task { get; set; }

        public DateTime ExecuteTime { get; set; }
        public string ExecuteResult { get; set; }

        public string ErrorMessage { get; set; }

    }
}
