﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KnifeZ.Byeol.Migrations
{
    public partial class updatetable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsInterrupt",
                table: "ByeolTasks",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "LoopSeconds",
                table: "ByeolTasks",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "NextExecuteTime",
                table: "ByeolTasks",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsInterrupt",
                table: "ByeolTasks");

            migrationBuilder.DropColumn(
                name: "LoopSeconds",
                table: "ByeolTasks");

            migrationBuilder.DropColumn(
                name: "NextExecuteTime",
                table: "ByeolTasks");
        }
    }
}
