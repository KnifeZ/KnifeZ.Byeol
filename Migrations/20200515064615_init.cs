﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KnifeZ.Byeol.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ByeolTasks",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    TaskID = table.Column<Guid>(nullable: false),
                    TaskName = table.Column<string>(maxLength: 100, nullable: true),
                    TaskGroupName = table.Column<string>(maxLength: 100, nullable: true),
                    AskMethodType = table.Column<int>(nullable: false),
                    AskApi = table.Column<string>(nullable: true),
                    AskParams = table.Column<string>(nullable: true),
                    Headers = table.Column<string>(nullable: true),
                    Remark = table.Column<string>(maxLength: 300, nullable: true),
                    IsEmail = table.Column<bool>(nullable: false),
                    EmailAddress = table.Column<string>(nullable: true),
                    CreateBy = table.Column<string>(nullable: true),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    UpdateBy = table.Column<string>(nullable: true),
                    UpdateTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ByeolTasks", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ByeolTasks");
        }
    }
}
