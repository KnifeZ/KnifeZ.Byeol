﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KnifeZ.Byeol.Migrations
{
    public partial class addrecordtable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ByeolTaskRecords",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    TaskID = table.Column<Guid>(nullable: false),
                    TaskID1 = table.Column<long>(nullable: true),
                    ExecuteTime = table.Column<DateTime>(nullable: false),
                    ExecuteResult = table.Column<string>(nullable: true),
                    ErrorMessage = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ByeolTaskRecords", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ByeolTaskRecords_ByeolTasks_TaskID1",
                        column: x => x.TaskID1,
                        principalTable: "ByeolTasks",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ByeolTaskRecords_TaskID1",
                table: "ByeolTaskRecords",
                column: "TaskID1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ByeolTaskRecords");
        }
    }
}
