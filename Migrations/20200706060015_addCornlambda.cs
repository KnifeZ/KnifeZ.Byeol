﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KnifeZ.Byeol.Migrations
{
    public partial class addCornlambda : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CronLambda",
                table: "ByeolTasks",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CronLambda",
                table: "ByeolTasks");
        }
    }
}
