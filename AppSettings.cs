﻿using KnifeZ.Unity;
using KnifeZ.Unity.WTM;
using System;
using System.Collections.Generic;
using System.Text;

namespace KnifeZ.Byeol
{
    public class AppSettings
    {
        /// <summary>
        /// 项目根目录
        /// </summary>
        public static string RootDirectory
        {
            get
            {
                var baseDir = AppDomain.CurrentDomain.BaseDirectory;
                return baseDir.Substring(0, baseDir.LastIndexOf("bin"));
            }
        }
#if DEBUG
        /// <summary>
        /// 程序默认数据库链接
        /// </summary>
        public static string DefaultConnectString = "Data Source=" + RootDirectory + "/Data/Beyol.db";
#endif
#if RELEASE
        /// <summary>
        /// 程序默认数据库链接
        /// </summary>
        public static string DefaultConnectString = "Data Source=" + AppDomain.CurrentDomain.BaseDirectory + "/Data/Beyol.db";
#endif
        /// <summary>
        /// 程序默认数据库类型
        /// </summary>
        public const DBTypeEnum DefalutDbType = DBTypeEnum.SQLite;
    }
}
