﻿using KnifeZ.Byeol.Models;
using KnifeZ.Byeol.Services;
using Microsoft.EntityFrameworkCore;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;

namespace KnifeZ.Byeol.ViewModels
{
    public class DashboardPageViewModel : BindableBase
    {
        private ByeolTask task;
        public ByeolTask Task
        {
            get { return task; }
            set { SetProperty(ref task, value); }
        }
        private ObservableCollection<ByeolTaskRecord> taskRecord;
        public ObservableCollection<ByeolTaskRecord> TaskRecord
        {
            get { return taskRecord; }
            set { SetProperty(ref taskRecord, value); }
        }
        public DelegateCommand DeleteTaskCommand { get; set; }
        public void DeleteTask()
        {
            using (var dc=new DataContext(AppSettings.DefaultConnectString))
            {
                dc.DeleteEntity<ByeolTask>(Task);
                dc.SaveChanges();
            }
            EventAggregatorRepository.GetInstance()
                .eventAggregator.GetEvent<PrismEventHandler<string>>()
                .Publish("RefreshTaskList");
        }
        public DelegateCommand InterruptTrueCommand { get; set; }
        public DelegateCommand InterruptFalseCommand { get; set; }
        public DelegateCommand RefreshCommand { get; set; }
        public void InterruptTaskTrue()
        {
            using (var dc = new DataContext(AppSettings.DefaultConnectString))
            {
                Task.IsInterrupt = true;
                dc.UpdateProperty(Task,"IsInterrupt");
                dc.SaveChanges();
            }
            EventAggregatorRepository.GetInstance()
                .eventAggregator.GetEvent<PrismEventHandler<string>>()
                .Publish("RefreshTaskList");

        }
        public void InterruptTaskFalse()
        {
            using (var dc = new DataContext(AppSettings.DefaultConnectString))
            {
                Task.IsInterrupt = false;
                dc.UpdateProperty(Task, "IsInterrupt");
                dc.SaveChanges();
            }
            EventAggregatorRepository.GetInstance()
                .eventAggregator.GetEvent<PrismEventHandler<string>>()
                .Publish("RefreshTaskList");

        }
        public void Refresh()
        {
            using var dc = new DataContext(AppSettings.DefaultConnectString);
            var btr = dc.Set<ByeolTaskRecord>().Where(x => x.TaskID == Task.TaskID).OrderByDescending(x => x.ExecuteTime).AsNoTracking().ToList();
            TaskRecord.AddRange(btr);
        }
        public DashboardPageViewModel(ByeolTask task)
        {
            TaskRecord = new ObservableCollection<ByeolTaskRecord>();
            Task = task;
            if (Task == null)
            {
                Task = new ByeolTask
                {
                    TaskGroupName = "Default"
                };
            }
            else
            {
                using var dc = new DataContext(AppSettings.DefaultConnectString);
                var btr = dc.Set<ByeolTaskRecord>().Where(x => x.TaskID == Task.TaskID).OrderByDescending(x => x.ExecuteTime).AsNoTracking().ToList();
                TaskRecord.AddRange(btr);
            }
            DeleteTaskCommand = new DelegateCommand(new Action(DeleteTask));
            InterruptFalseCommand = new DelegateCommand(new Action(InterruptTaskFalse));
            RefreshCommand = new DelegateCommand(new Action(Refresh));
            InterruptTrueCommand = new DelegateCommand(new Action(InterruptTaskTrue));
        }
        public DashboardPageViewModel()
        {
            Task = new ByeolTask
            {
                TaskGroupName = "Default",
                IsEmail = false,
                IsInterrupt = false,
                LoopSeconds = 86400,
                CreateTime = DateTime.Now,
                UpdateTime = DateTime.Now
            };
            TaskRecord = new ObservableCollection<ByeolTaskRecord>();
            DeleteTaskCommand = new DelegateCommand(new Action(DeleteTask));
        }
    }
}
