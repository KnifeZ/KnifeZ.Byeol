﻿using KnifeZ.Byeol.Models;
using KnifeZ.Byeol.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KnifeZ.Byeol.Views
{
    /// <summary>
    /// DashboardPage.xaml 的交互逻辑
    /// </summary>
    public partial class DashboardPage : Page
    {
        public DashboardPage()
        {
            InitializeComponent();
            DataContext = new DashboardPageViewModel();
            this.DashBoardPannel.Visibility = Visibility.Visible;
            this.TaskGrid.Visibility = Visibility.Hidden;
        }
        public DashboardPage(ByeolTask byeolTask)
        {
            InitializeComponent();
            DataContext = new DashboardPageViewModel(byeolTask);
            this.DashBoardPannel.Visibility = Visibility.Hidden;
            this.DashBoardPannel.Height = 0;
            this.TaskGrid.Visibility = Visibility.Visible;
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            var dc = this.DataContext as DashboardPageViewModel;
            NavigationService.Navigate(new TaskModifyPage(dc.Task));
        }
    }
}
