﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Navigation;

namespace KnifeZ.Byeol.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        WindowState ws;
        public MainWindow()
        {
            InitializeComponent();
        }
        private void Window_StateChanged(object sender, EventArgs e)
        {
            ws = WindowState;
            if (ws == WindowState.Minimized)
            {
                this.Hide();
            }
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            if (MessageBox.Show("确定要关闭吗？关闭后任务不再执行！", "注意", MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.Yes)
            {
                base.OnClosing(e);
            }
            else
            {
                e.Cancel = true;
                this.Hide();
            }
        }
        private void ListView_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0)
            {
                MainFrame.Navigate(new DashboardPage());
            }
            else
            {
                var model = e.AddedItems[0] as Models.ByeolTask;
                MainFrame.Navigate(new DashboardPage(model));

            }
        }

        private void AddTask_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new TaskModifyPage());
        }
    }
}
