﻿using KnifeZ.Byeol.Models;
using KnifeZ.Byeol.ViewModels;
using KnifeZ.Unity.Helper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KnifeZ.Byeol.Views
{
    /// <summary>
    /// TaskModify.xaml 的交互逻辑
    /// </summary>
    public partial class TaskModifyPage : Page
    {
        public TaskModifyPage()
        {
            InitializeComponent();
            DataContext = new TaskModifyPageViewModel();
        }

        public TaskModifyPage(ByeolTask task)
        {
            InitializeComponent();
            DataContext = new TaskModifyPageViewModel(task);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }
    }
}
