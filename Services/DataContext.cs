﻿using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;
using KnifeZ.Unity.WTM;
using KnifeZ.Unity;
using Microsoft.EntityFrameworkCore;
using KnifeZ.Byeol.Models;

namespace KnifeZ.Byeol.Services
{

    public class DataContext : FrameworkDataContext
    {
        // Tables
        public DbSet<ByeolTask> ByeolTasks { get; set; }
        public DbSet<ByeolTaskRecord> ByeolTaskRecords { get; set; }
        // end
        public DataContext(string cs, DBTypeEnum dbtype = AppSettings.DefalutDbType)
             : base(cs, dbtype)
        {

        }

    }
    public class DataContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {
            return new DataContext(AppSettings.DefaultConnectString, AppSettings.DefalutDbType);
        }
    }

}
