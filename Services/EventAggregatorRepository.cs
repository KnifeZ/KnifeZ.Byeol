﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.Text;


namespace KnifeZ.Byeol.Services
{
    public class EventAggregatorRepository
    {
        public EventAggregatorRepository()
        {
            eventAggregator = new EventAggregator();
        }

        public IEventAggregator eventAggregator;
        public static EventAggregatorRepository eventRepository = null;

        //单例，保持内存唯一实例
        public static EventAggregatorRepository GetInstance()
        {
            if (eventRepository == null)
            {
                eventRepository = new EventAggregatorRepository();
            }
            return eventRepository;
        }
    }

    /// <summary>
    /// 自定义的事件，不做任何实现
    /// </summary>
    public class PrismEventHandler<Tpayload> : PubSubEvent<Tpayload>
    {

    }

}
